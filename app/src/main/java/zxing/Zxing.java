package zxing;

import android.graphics.Bitmap;
import android.graphics.Color;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import java.util.Hashtable;

/**
 * QRコード作成クラス
 */
public class Zxing {
    /**
     * QRコードを作成する。
     * @param contents 作成するQRコードの内容
     * @param size 作成するQRコードのサイズ
     * @return QRコードのBitmap
     * @see zxing
     */
    public Bitmap createQR(String contents, int size) throws WriterException {
        QRCodeWriter writer = new QRCodeWriter();
        Hashtable<EncodeHintType, java.io.Serializable> encodeHint = new Hashtable<>();
        encodeHint.put(EncodeHintType.CHARACTER_SET, "shiftjis");
        encodeHint.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
        BitMatrix qrCodeData = writer.encode(contents, BarcodeFormat.QR_CODE, size, size, encodeHint);
        Bitmap bitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
        for (int x = 0; x < qrCodeData.getWidth(); x++) {
            for (int y = 0; y < qrCodeData.getHeight(); y++) {
                if (qrCodeData.get(x, y) == true) {
                    bitmap.setPixel(x, y, Color.argb(255, 0, 0, 0));
                } else {
                    bitmap.setPixel(x, y, Color.argb(255, 255, 255, 255));
                }
            }
        }
        return bitmap;
    }
}
