package com.example.nakatsu.gbqr;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.print.PrintHelper;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.google.zxing.WriterException;

import zxing.Zxing;

public class QRActivity extends AppCompatActivity {
    Bitmap currentBitmap;
    ProgressDialog progressDialog;

    @SuppressLint("StaticFieldLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr);
        Button button4 = findViewById(R.id.button4);
        button4.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                pickFilenameFromGallery();
            }
        });

        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("QRコードを作成しています。");
        progressDialog.setCancelable(true);
        progressDialog.show();

        final ImageView imageView = findViewById(R.id.imageView);
        new AsyncTask<Integer,Bitmap,Bitmap>() {
            @Override
            protected Bitmap doInBackground(Integer... integer) {
                Intent intent = getIntent();
                String data = intent.getStringExtra("QRContents");
                return makeQR(data);
            }

            @Override
            protected void onPostExecute(Bitmap result) {
                imageView.setImageBitmap(result);
                progressDialog.cancel();
            }
        }.execute(1);
    }

    private void pickFilenameFromGallery() {
        PrintHelper printer = new PrintHelper(getBaseContext());
        printer.printBitmap("QR",currentBitmap);
    }

    private Bitmap makeQR(String textQR) {
        Bitmap bitmap = null;
        try {
            Zxing zxing = new Zxing();
            bitmap = zxing.createQR(textQR, 500);
            currentBitmap = bitmap;
        } catch (WriterException e) {
            e.printStackTrace();
        }
        return bitmap;
    }
}
