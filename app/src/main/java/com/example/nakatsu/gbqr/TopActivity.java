package com.example.nakatsu.gbqr;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TopActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top);
        Button button = findViewById(R.id.button2);
        button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				toQR();
			}
		});

        Spinner spinner = findViewById(R.id.spinner);

        ArrayAdapter adapter = new ArrayAdapter<>(this,
                R.layout.list, new String[] {"商品名称","アクセサリー・小物","酒類"});
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
    }

    public void toQR() {
        Intent intent = new Intent(this, QRActivity.class);
        String number = ((EditText)findViewById(R.id.editText2)).getText().toString();
        String MoneyIncludeTax = ((EditText)findViewById(R.id.editText3)).getText().toString();
        String money = ((EditText)findViewById(R.id.editText4)).getText().toString();
        Spinner spinner = findViewById(R.id.spinner);
        int item = (int)spinner.getSelectedItemId();
        String itemName = (String) spinner.getSelectedItem();
        RadioButton radioShomou = findViewById(R.id.radioButton2);
        String category = radioShomou.isSelected() ? "Gross" : "Net";
        try {
            FileInputStream in = openFileInput("GB.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
            StringBuilder str = new StringBuilder();
            String tmp;
            while ((tmp = reader.readLine()) != null) {
                str.append(tmp).append("\n");
            }
            reader.close();
            String[] split = str.toString().split("\n");
            String shopID = "";
            String deskID = "";
            if (split.length == 2) {
                shopID = split[0];
                deskID = split[1];
            }

            Date date = new Date();
            @SuppressLint("SimpleDateFormat")
			SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyyMMdd");
			@SuppressLint("SimpleDateFormat")
			SimpleDateFormat simpleFormat2 = new SimpleDateFormat("yyyy-MM-dd");
			@SuppressLint("SimpleDateFormat")
			SimpleDateFormat simpleFormatTime = new SimpleDateFormat("hh:mm:ss");
            intent.putExtra("QRContents", "Q~" + simpleFormat.format(date)
                    + "~~" + shopID + "~" + deskID + "~~"
                    + simpleFormat2.format(date) + "T" + simpleFormatTime.format(date) + "~"
                    + number + "~" + item + "~" + itemName + "~" + number + "~"
                    + MoneyIncludeTax + "~" + money + "~~8.00~" + category + "~NonConsumable");
        } catch (IOException e) {
            e.printStackTrace();
        }
        startActivityForResult(intent, 12);
    }
}
