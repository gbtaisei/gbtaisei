package com.panasonic.avc.smartpayment.devctlservice.share;

import com.panasonic.avc.smartpayment.devctlservice.share.IPprServiceListener;
import com.panasonic.avc.smartpayment.devctlservice.share.result.ResultGetPackageInfo;
import com.panasonic.avc.smartpayment.devctlservice.share.result.ppr.ResultInitPPR;
import com.panasonic.avc.smartpayment.devctlservice.share.result.ppr.ResultSendPPR;
import com.panasonic.avc.smartpayment.devctlservice.share.result.ppr.ResultTermPPR;

/**
 * PPR Service AIDL
 * @author thamada
 *
 */
interface IPprService {

    /**
     * PPR Pluginのリスナーを登録します
     * @param[in] listener listener
     */
    void registerPPRServiceListener(String tag,IPprServiceListener listener);
    
    /**
     * PPR Pluginのリスナーを登録解除します
     * @param[in] listener 解除対象のlistener
     */
    void unregisterPPRServiceListener(String tag);
        
    /**
     * @brief PPRを初期化します
     * @return 実行結果
     */
    ResultInitPPR initPPR();

    /**
     * @brief パスポートリーダにデータを送信します
     * @param[in] datasz 送信データサイズ
     * @param[in] data 送信データの16進文字列
     * @return 実行結果
     */
    ResultSendPPR sendPPR(int datasz, String data);
    
    /**
     * @brief パッケージ情報を取得します
     * @return 実行結果
     * @retval package パッケージ情報[name,ver]
     * @retval name パッケージ名[文字列型]
     * @retval ver パッケージバージョン[文字列型]
     */
    ResultGetPackageInfo getPackageInfo(String jsName, String jsVer, String pluginName,String pluginVer);
    
    /**
     * @brief パスポートリーダをターミネートします
     * @return 実行結果
     */
    ResultTermPPR termPPR();
    
}
