package com.example.nakatsu.gbqr;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.FileOutputStream;
import java.io.IOException;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button = findViewById(R.id.button3);
        button.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        EditText shopID = findViewById(R.id.editTextShopID);
        EditText deskID = findViewById(R.id.editTextDeskID);
        try {
			String str = shopID.getText().toString() + "\n" + deskID.getText().toString();
			FileOutputStream out = openFileOutput( "GB.txt", MODE_PRIVATE );
			out.write( str.getBytes() );
		} catch (IOException e) {
            e.printStackTrace();
        }
        Intent intent = new Intent(this, TopActivity.class);
        startActivityForResult(intent, 12);
    }
}
